package learn.algo.codeleetcode;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class L0242ValidAnagramTest {

    @ParameterizedTest
    @CsvSource({"anagram,nagaram,true", "rat,car,false"})
    void isAnagram(String s, String t, boolean expected) {
        boolean result = L0242ValidAnagram.isAnagram(s, t);
        assertEquals(expected, result);
    }
}
