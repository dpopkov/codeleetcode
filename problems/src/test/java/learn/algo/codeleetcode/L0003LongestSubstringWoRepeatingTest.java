package learn.algo.codeleetcode;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class L0003LongestSubstringWoRepeatingTest {

    private static Stream<Arguments> testData() {
        return Stream.of(
                Arguments.of("aba", 2),
                Arguments.of("abcabcbb", 3),
                Arguments.of("bbbbb", 1),
                Arguments.of("pwwkew", 3),
                Arguments.of("pwwwpkew", 4),
                Arguments.of("pwwkewz", 4)
        );
    }

    @ParameterizedTest
    @MethodSource("testData")
    void longestSubstring(String input, int expectedLength) {
        int actual = new L0003LongestSubstringWoRepeating().lengthOfLongestSubstring(input);
        assertEquals(expectedLength, actual);
    }
}
