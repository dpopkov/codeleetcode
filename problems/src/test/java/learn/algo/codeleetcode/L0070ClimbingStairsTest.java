package learn.algo.codeleetcode;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class L0070ClimbingStairsTest {

    @ParameterizedTest
    @CsvSource({
            "2,2",
            "3,3",
            "4,5",
            "5,8"
    })
    void climbStairs(int input, int expected) {
        int actual = L0070ClimbingStairs.climbStairs(input);
        assertEquals(expected, actual);
    }
}
