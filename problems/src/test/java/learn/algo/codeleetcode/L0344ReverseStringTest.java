package learn.algo.codeleetcode;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class L0344ReverseStringTest {

    private static Stream<Arguments> testData() {
        return Stream.of(
                Arguments.of("hello", "olleh"),
                Arguments.of("Hannah", "hannaH")
        );
    }

    @ParameterizedTest
    @MethodSource("testData")
    void reverseString(String input, String expected) {
        final char[] chars = input.toCharArray();
        new L0344ReverseString().reverseString(chars);
        String actual = String.valueOf(chars);
        assertEquals(expected, actual);
    }
}
