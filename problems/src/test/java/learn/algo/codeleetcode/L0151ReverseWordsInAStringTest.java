package learn.algo.codeleetcode;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class L0151ReverseWordsInAStringTest {

    private static Stream<Arguments> testData() {
        return Stream.of(
                Arguments.of("the sky is blue", "blue is sky the"),
                Arguments.of("a good   example", "example good a"),
                Arguments.of("  a good   example", "example good a"),
                Arguments.of("a good example  ", "example good a")
        );
    }

    @ParameterizedTest
    @MethodSource("testData")
    void reverseWords(String input, String expected) {
        String actual = L0151ReverseWordsInAString.reverseWords(input);
        assertEquals(expected, actual);
    }
}
