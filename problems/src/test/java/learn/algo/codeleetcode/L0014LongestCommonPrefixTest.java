package learn.algo.codeleetcode;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class L0014LongestCommonPrefixTest {

    private static Stream<Arguments> testData() {
        return Stream.of(
                Arguments.of(new String[] {"a", "ac"}, "a"),
                Arguments.of(new String[] {"ab", "ac"}, "a"),
                Arguments.of(new String[] {"ab", "a"}, "a"),
                Arguments.of(new String[] {"flower", "flow", "flight"}, "fl"),
                Arguments.of(new String[] {"dog", "race", "car"}, ""),
                Arguments.of(new String[] {""}, "")
        );
    }

    @ParameterizedTest
    @MethodSource("testData")
    void longestCommonPrefix(String[] input, String expected) {
        String actual = new L0014LongestCommonPrefix().longestCommonPrefix(input);
        assertEquals(expected, actual);
    }
}
