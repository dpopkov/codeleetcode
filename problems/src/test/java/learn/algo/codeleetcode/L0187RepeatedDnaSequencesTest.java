package learn.algo.codeleetcode;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class L0187RepeatedDnaSequencesTest {

    private static Stream<Arguments> testDataLength2() {
        return Stream.of(
                Arguments.of("AACCAACCAA", Set.of("AA", "AC", "CC", "CA")),
                Arguments.of("AACCAAGGCCAAGG", Set.of("AA", "CC", "CA", "AG", "GG"))
        );
    }

    private static Stream<Arguments> testDataLengthDefault() {
        return Stream.of(
                Arguments.of("AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT", Set.of("AAAAACCCCC", "CCCCCAAAAA")),
                Arguments.of("AAAAAAAAAAAAA", Set.of("AAAAAAAAAA"))
        );
    }

    @ParameterizedTest
    @MethodSource("testDataLength2")
    void findRepeatedDnaSequencesLength2(String input, Set<String> expected) {
        List<String> actual = new L0187RepeatedDnaSequences(2).findRepeatedDnaSequences(input);
        assertEquals(expected.size(), actual.size());
        assertEquals(expected, Set.copyOf(actual));
    }

    @ParameterizedTest
    @MethodSource("testDataLengthDefault")
    void findRepeatedDnaSequencesLength10(String input, Set<String> expected) {
        List<String> actual = new L0187RepeatedDnaSequences().findRepeatedDnaSequences(input);
        assertEquals(expected.size(), actual.size());
        assertEquals(expected, Set.copyOf(actual));
    }
}
