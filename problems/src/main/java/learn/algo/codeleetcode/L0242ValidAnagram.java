package learn.algo.codeleetcode;

/*
Given two strings s and t, return true if t is an anagram of s, and false otherwise.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase,
typically using all the original letters exactly once.

Constraints:
    1 <= s.length, t.length <= 5 * 10^4
    s and t consist of lowercase English letters.
 */
public class L0242ValidAnagram {

    public static boolean isAnagram(String s, String t) {
        // implement
        return false;
    }
}
