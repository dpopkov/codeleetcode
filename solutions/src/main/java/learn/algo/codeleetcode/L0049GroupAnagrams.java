package learn.algo.codeleetcode;


import java.util.*;

/*
Group Anagrams.

Given an array of strings, group the anagrams together.
You can return the answer in any order.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase,
typically using all the original letters exactly once.
 */
public class L0049GroupAnagrams {

    public List<List<String>> groupAnagrams(String[] strs) {
        Map<String, List<String>> groups = new HashMap<>();
        for (String s : strs) {
            char[] chars = s.toCharArray();
            Arrays.sort(chars);
            String sortedChars = new String(chars);
            List<String> list = groups.computeIfAbsent(sortedChars, k -> new ArrayList<>());
            list.add(s);
        }
        return new ArrayList<>(groups.values());
    }
}
