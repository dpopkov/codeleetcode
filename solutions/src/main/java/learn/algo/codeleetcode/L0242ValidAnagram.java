package learn.algo.codeleetcode;

import java.util.HashMap;
import java.util.Map;

/*
Given two strings s and t, return true if t is an anagram of s, and false otherwise.

An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase,
typically using all the original letters exactly once.

Constraints:
    1 <= s.length, t.length <= 5 * 10^4
    s and t consist of lowercase English letters.
 */
public class L0242ValidAnagram {

    public static boolean isAnagram(String s, String t) {
//        return solutionUsingHashMap(s, t); // 1st solution (10ms, 59.96mb)
        return solutionUsingArray(s, t);     // 2nd solution (2ms, 42.2mb)
    }

    // 2nd solution
    private static boolean solutionUsingArray(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        final int[] counts = new int[26];
        final int len = s.length();
        for (int i = 0; i < len; i++) {
            counts[s.charAt(i) - 'a']++;
        }
        for (int j = 0; j < len; j++) {
            int idx = t.charAt(j) - 'a';
            counts[idx]--;
            if (counts[idx] < 0) {
                return false;
            }
        }
        return true;
    }

    // 1st solution
    private static boolean solutionUsingHashMap(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        Map<Character, Integer> visited = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            visited.merge(s.charAt(i), 1, (v0, v1) -> v0 + 1);
        }
        for (int i = 0; i < t.length(); i++) {
            char key = t.charAt(i);
            Integer count = visited.get(key);
            if (count == null || count == 0) {
                return false;
            }
            if (count == 1) {
                visited.remove(key);
            } else {
                visited.put(key, count - 1);
            }
        }
        return visited.isEmpty();
    }

}
