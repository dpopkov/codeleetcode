package learn.algo.codeleetcode;

import java.util.*;

/*
The DNA sequence is composed of a series of nucleotides abbreviated as 'A', 'C', 'G', and 'T'.
For example, "ACGAATTCCG" is a DNA sequence.
When studying DNA, it is useful to identify repeated sequences within the DNA.
Given a string s that represents a DNA sequence, return all the 10-letter-long
sequences (substrings) that occur more than once in a DNA molecule.
You may return the answer in any order.
 */
public class L0187RepeatedDnaSequences {

    private static final int DEFAULT_LENGTH = 10;

    private final int length;

    public L0187RepeatedDnaSequences() {
        this.length = DEFAULT_LENGTH;
    }

    public L0187RepeatedDnaSequences(int length) {
        this.length = length;
    }

    public List<String> findRepeatedDnaSequences(String s) {
        Set<String> set = new HashSet<>();
        int finish = s.length() - length;
        final Set<String> result = new HashSet<>();
        for (int start = 0; start <= finish; start++) {
            String sub = s.substring(start, start + length);
            if (set.contains(sub)) {
                result.add(sub);
            } else {
                set.add(sub);
            }
        }
        return new ArrayList<>(result);
    }

    public List<String> findRepeatedDnaSequencesMap(String s) {
        final Integer one = 1;
        Map<String, Integer> map = new HashMap<>();
        int finish = s.length() - length;
        for (int start = 0; start <= finish; start++) {
            String sub = s.substring(start, start + length);
            map.compute(sub, (k, v) -> v == null ? one : v + 1);
        }
        List<String> result = new ArrayList<>();
        map.forEach((k, v) -> {
            if (!one.equals(v)) {
                result.add(k);
            }
        });
        return result;
    }
}
