package learn.algo.codeleetcode;

import java.util.ArrayDeque;
import java.util.Deque;

/*
Given an input string s, reverse the order of the words.

A word is defined as a sequence of non-space characters.
The words in s will be separated by at least one space.

Return a string of the words in reverse order concatenated by a single space.

Note that s may contain leading or trailing spaces or multiple spaces between two words.
The returned string should only have a single space separating the words. Do not include any extra spaces.
 */
public class L0151ReverseWordsInAString {

    public static String reverseWords(String s) {
//        return reverseUsingStack(s);  // 1st solution (4ms, 41.6mb)
        return reverseGoingRightToLeft(s);  // 2nd solution (3ms, 41.9mb) looks best
//        return reverseTwiceAndCleanSpaces(s); // 3rd solution (3ms, 42.4mb)
    }

    // 3rd Solution
    private static String reverseTwiceAndCleanSpaces(String s) {
        char[] chars = s.toCharArray();
        reverseRange(chars, 0, chars.length - 1);
        reverseWords(chars);
        return cleanSpaces(chars);
    }

    private static void reverseWords(char[] chars) {
        for (int i = 0; i < chars.length; i++) {
            while (i < chars.length && chars[i] == ' ') {
                i++;
            }
            if (i == chars.length) {
                break;
            }
            // i points to start of word
            int j = i + 1;
            while (j < chars.length && chars[j] != ' ') {
                j++;
            }
            // j points to space on the right of word or beyond array
            reverseRange(chars, i, j - 1);
            i = j;
        }
    }

    static String cleanSpaces(char[] chars) {
        int i = 0;
        int j = 0;
        while (j < chars.length) {
            while (j < chars.length && chars[j] == ' ') {
                j++;
            }
            // j points to letter or End
            /*if (j == chars.length) {
                break;
            }*/
            while (j < chars.length && chars[j] != ' ') {
                chars[i++] = chars[j++];
            }
            // j points to space or End
            while (j < chars.length && chars[j] == ' ') {
                j++;
            }
            // j points to letter or End
            if (j < chars.length) {
                chars[i++] = ' ';
            }
        }
        return new String(chars, 0, i);
    }

    private static void reverseRange(char[] chars, int first, int last) {
        char t;
        for (int i = first, j = last; i < j; i++, j--) {
            t = chars[i];
            chars[i] = chars[j];
            chars[j] = t;
        }
    }

    // 2nd Solution
    private static String reverseGoingRightToLeft(String s) {
        StringBuilder sb = new StringBuilder();
        int right = s.length() - 1;
        while (right >= 0) {
            while (right >= 0 && s.charAt(right) == ' ') {
                right--;
            }
            // i points to non-whitespace or -1
            if (right == -1) {
                break;
            }
            int left = right - 1;
            while (left >= 0 && s.charAt(left) != ' ') {
                left--;
            }
            // j points to white-space or -1
            if (sb.length() > 0) {
                sb.append(' ');
            }
            sb.append(s, left + 1, right + 1);
            // j points to non-whitespace or 0
            right = left;
        }
        return sb.toString();
    }

    // 1st Solution
    private static String reverseUsingStack(String s) {
        Deque<String> stack = new ArrayDeque<>();
        final int len = s.length();
        int i = 0;
        while (i < len) {
            // go until non-whitespace or end
            while (i < len && Character.isWhitespace(s.charAt(i))) {
                i++;
            }
            // i points to non-whitespace or end
            if (i == len) {
                break;
            }
            int j = i + 1;
            // go until whitespace or end
            while (j < len && !Character.isWhitespace(s.charAt(j))) {
                j++;
            }
            // j points to whitespace or end
            stack.push(s.substring(i, j));
            i = j + 1;
        }
        StringBuilder sb = new StringBuilder();
        while (!stack.isEmpty()) {
            if (sb.length() > 0) {
                sb.append(' ');
            }
            sb.append(stack.pop());
        }
        return sb.toString();
    }
}
