package learn.algo.codeleetcode;

/*
Given two binary strings a and b, return their sum as a binary string.
 */
public class L0067AddBinary {

    public static String addBinary(String a, String b) {
        if ("0".equals(a)) {
            return b;
        } else if ("0".equals(b)) {
            return a;
        }
        final int aLen = a.length();
        final int bLen = b.length();
        char[] digits = new char[Math.max(aLen, bLen) + 1];
        int iA = aLen - 1;
        int iB = bLen - 1;
        char p = '0';
        char digit;
        for (int i = digits.length - 1; i != 0 ; i--) {
            if (iA >= 0 && iB >= 0) {
                char dA = a.charAt(iA--);
                char dB = b.charAt(iB--);
                if (dA == '1' && dB == '1') {
                    digit = p;
                    p = '1';
                } else if (dA == '0' && dB == '0') {
                    digit = p;
                    p = '0';
                } else {
                    if (p == '1') {
                        digit = '0';
                    } else {
                        digit = '1';
                    }
                }
            } else if (iA >= 0) {
                if (a.charAt(iA) == '1') {
                    if (p == '1') {
                        digit = '0';
                    } else {
                        digit = '1';
                    }
                } else {
                    digit = p;
                    p = '0';
                }
                iA--;
            } else {
                if (b.charAt(iB) == '1') {
                    if (p == '1') {
                        digit = '0';
                    } else {
                        digit = '1';
                    }
                } else {
                    digit = p;
                    p = '0';
                }
                iB--;
            }
            digits[i] = digit;
        }
        digits[0] = p;
        int offset = 0;
        while (offset < digits.length && digits[offset] == '0') {
            offset++;
        }
        return new String(digits, offset, digits.length - offset);
    }
}
