package learn.algo.codeleetcode;

import java.util.HashMap;
import java.util.Map;

/*
3. Longest Substring Without Repeating Characters.
Given a string s, find the length of the longest substring without repeating characters.
 */
public class L0003LongestSubstringWoRepeating {

    // TC: O(n), SC: O(n)
    public int lengthOfLongestSubstring(String s) {
        Map<Character, Integer> map = new HashMap<>();
        int first = 0;
        int maxLength = 0;
        for (int last = 0; last < s.length(); last++) {
            char ch = s.charAt(last);
            if (map.containsKey(ch)) {
                first = Math.max(first, map.get(ch) + 1);
            }
            map.put(ch, last);
            maxLength = Math.max(maxLength, last - first + 1);
        }
        return maxLength;
    }

    // Old solution that passes the LeetCode test, but fails mine.
    public int lengthOfLongestSubstringOld(String s) {
        Map<Character, Integer> map = new HashMap<>();
        int first = 0;
        int maxLength = 0;
        for (int last = 0; last < s.length(); last++) {
            char ch = s.charAt(last);
            if (map.containsKey(ch)) {
                first = map.get(ch) + 1;
            }
            map.put(ch, last);
            maxLength = Math.max(maxLength, last - first + 1);
        }
        return maxLength;
    }
}
