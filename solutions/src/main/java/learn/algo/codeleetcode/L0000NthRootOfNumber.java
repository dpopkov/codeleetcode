package learn.algo.codeleetcode;

/*
Find n-th root of x.
For example:
x = 27
n = 3
find y such that y^3 = 27
 */
public class L0000NthRootOfNumber {

    static final double ALLOWED_ERROR = 1e-3;
    private static final double INITIAL_LEFT = 1.0;

    public double nthRoot(double x, int n) {
        if (x <= INITIAL_LEFT) {
            throw new IllegalArgumentException("This value is not legal for the function: " + x);
        }
        double left = INITIAL_LEFT;
        double right = x;
        while (true) {
            double middle = left + (right - left) / 2;
            double check = Math.pow(middle, n);
            if (Math.abs(check - x) <= ALLOWED_ERROR) {
                return middle;
            }
            if (check < x) {
                left = middle;
            } else if (check > x) {
                right = middle;
            }
        }
    }
}
