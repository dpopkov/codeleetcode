package learn.algo.codeleetcode;

/*
Given an encode string, return its decoded string.

The encoding rule is: k[encoded_string], where the encoded_string
inside the square brackets is being repeated exactly k times.
Note that k is guaranteed to be a positive integer.

You may assume that the input string is always valid;
there are no extra white spaces, square brackets are well-formed, etc.
Furthermore, you may assume that the original data does not contain any digits
and that digits are only for those repeated numbers, k.
For example, there will not be input like 3a or 2[4].

The test cases are generated so that the length of the output will never exceed 10^5.

Example:
Input: s = "3[a]2[bc]"
Output: "aaabcbc"
 */

import java.util.Deque;
import java.util.LinkedList;

public class L0394DecodeString {

    public static String decodeString(String s) {
//        return new SimpleDecoder().decodeString(s);
        return new DecoderWith2Stacks().decodeString(s);
    }

    private static class DecoderWith2Stacks {

        public String decodeString(String s) {
            final Deque<Integer> numStack = new LinkedList<>();
            final Deque<String> strStack = new LinkedList<>();
            final StringBuilder sb = new StringBuilder();
            final int len = s.length();
            for (int i = 0; i < len; i++) {
                char ch = s.charAt(i);
                if (Character.isDigit(ch)) {
                    int number = ch - '0';
                    while (i + 1 < len && Character.isDigit(s.charAt(i + 1))) {
                        number = number * 10 + s.charAt(i + 1) - '0';
                        i++;
                    }
                    numStack.push(number);
                } else if ('[' == ch) {
                    strStack.push(sb.toString());
                    sb.setLength(0);
                } else if (']' == ch) {
                    String tmp = strStack.pop();
                    Integer n = numStack.pop();
                    tmp += sb.toString().repeat(n);
                    sb.setLength(0);
                    sb.append(tmp);
                } else {
                    sb.append(ch);
                }
            }
            return sb.toString();
        }
    }

    @SuppressWarnings("unused")
    private static class SimpleDecoder {
        private int currentIdx;

        public String decodeString(String s) {
            currentIdx = 0;
            final StringBuilder builder = new StringBuilder();
            while (currentIdx < s.length()) {
                String decoded = decodeExpression(s);
                builder.append(decoded);
            }
            return builder.toString();
        }

        private String decodeExpression(String s) {
            int from = currentIdx;
            if (Character.isDigit(s.charAt(currentIdx))) {
                int n = readNumber(s, from);
                String inBrackets = decodeInBrackets(s);
                return inBrackets.repeat(n);
            } else {
                return readLetters(s, from);
            }
        }

        private String decodeInBrackets(String s) {
            currentIdx++; // eat opening bracket
            StringBuilder sb = new StringBuilder(decodeExpression(s));
            while (Character.isDigit(s.charAt(currentIdx)) || Character.isAlphabetic(s.charAt(currentIdx))) {
                String inner = decodeExpression(s);
                sb.append(inner);
            }
            if (']' == s.charAt(currentIdx)) {
                currentIdx++; // eat closing bracket
            } else {
                throw new IllegalArgumentException("Invalid closing char at index " + currentIdx + " in string " + s);
            }
            return sb.toString();
        }

        private int readNumber(String s, int from) {
            while (currentIdx < s.length() && Character.isDigit(s.charAt(currentIdx))) {
                currentIdx++;
            }
            return Integer.parseInt(s.substring(from, currentIdx));
        }

        private String readLetters(String s, int from) {
            while (currentIdx < s.length() && Character.isAlphabetic(s.charAt(currentIdx))) {
                currentIdx++;
            }
            return s.substring(from, currentIdx);
        }
    }
}
