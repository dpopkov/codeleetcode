package learn.algo.codeleetcode;

/*
Convert a non-negative integer num to its English words representation.
Constraints:
    0 <= num <= 2^31 - 1
 */
public class L0273IntegerToEnglishWords {

    private static final String[] LESS_20 = {
            null,
            "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten",
            "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"
    };
    private static final String[] TENS = {
            null, null,
            "Twenty", "Thirty", "Forty", "Fifty",
            "Sixty", "Seventy", "Eighty", "Ninety"
    };
    private static final String[] THOUSANDS = {
        null, "Thousand", "Million", "Billion"
    };

    public static String numberToWords(int num) {
        if (num == 0) {
            return "Zero";
        }
        int thousandsIdx = 0;
        String result = "";
        do {
            String tmp = processThreeDigits(num % 1000, thousandsIdx++);
            result = concatenateWithSpace(tmp, result);
            num = num / 1000;
        } while (num != 0);
        return result;
    }

    private static String lessThanTwenty(int num) {
        return num != 0 ? LESS_20[num] : "";
    }

    private static String lessThanHundred(int num) {
        if (num < 20) {
            return lessThanTwenty(num);
        }
        int t = num / 10;
        int r = num % 10;
        String s1 = TENS[t];
        String s2 = lessThanTwenty(r);
        return concatenateWithSpace(s1, s2);
    }

    private static String toThousand(int num) {
        if (num < 100) {
            return lessThanHundred(num);
        }
        String s1 = withSuffix(lessThanHundred(num / 100), "Hundred");
        String s2 = lessThanHundred(num % 100);
        return concatenateWithSpace(s1, s2);
    }

    private static String processThreeDigits(int threeDigits, int thousandsIdx) {
        String s = toThousand(threeDigits);
        if (thousandsIdx != 0) {
            s = withSuffix(s, THOUSANDS[thousandsIdx]);
        }
        return s;
    }

    private static String concatenateWithSpace(String s1, String s2) {
        if (s1 == null || s1.isEmpty()) {
            return s2;
        } else {
            if (s2 == null || s2.isEmpty()) {
                return s1;
            } else {
                return s1 + " " + s2;
            }
        }
    }

    private static String withSuffix(String s, String suffix) {
        return s.isEmpty() ? "" : (s + " " + suffix);
    }
}
