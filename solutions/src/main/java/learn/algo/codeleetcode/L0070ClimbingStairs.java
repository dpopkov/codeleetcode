package learn.algo.codeleetcode;

/*
You are climbing a staircase. It takes n steps to reach the top.

Each time you can either climb 1 or 2 steps.
In how many distinct ways can you climb to the top?
 */
public class L0070ClimbingStairs {

    public static int climbStairs(int n) {
        return fibonacciSolution(n);
    }

    private static int fibonacciSolution(int n) {
        int prev = 1;
        int current = 1;
        for (int i = 1; i < n; i++) {
            int next = prev + current;
            prev = current;
            current = next;
        }
        return current;
    }

    private static int naiveRecursiveSolution(int n) {
        if (n < 4) {
            return n;
        } else {
            return naiveRecursiveSolution(n - 1) + naiveRecursiveSolution(n - 2);
        }
    }
}
