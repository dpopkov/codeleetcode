package learn.algo.codeleetcode;

/*
Given a sorted array of distinct integers and a target value,
return the index if the target is found.
If not, return the index where it would be if it were inserted in order.

You must write an algorithm with O(log n) runtime complexity.
 */
public class L0035SearchInsertPosition {

    public static int searchInsert(int[] nums, int target) {
        int low = 0;
        int hi = nums.length - 1;
        int middle;
        while (low <= hi) {
            middle = low + (hi - low) / 2;
            if (nums[middle] == target) {
                return middle;
            } else if (target < nums[middle]) {
                hi = middle - 1;
            } else {
                low = middle + 1;
            }
        }
        return low;
    }
}
