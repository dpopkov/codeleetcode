package learn.algo.codeleetcode;

/*
Given a signed 32-bit integer x, return x with its digits reversed.
If reversing x causes the value to go outside the signed 32-bit integer range [-2^31, 2^31 - 1], then return 0.
 */
public class L0007ReverseInteger {

    public int reverse(int x) {
        final int max10 = Integer.MAX_VALUE / 10;
        final int lastDigitOfMaximum = Integer.MAX_VALUE % 10;
        final int min10 = Integer.MIN_VALUE / 10;
        final int lastDigitOfMinimum = Integer.MIN_VALUE % 10;
        int result = 0;
        while (x != 0) {
            int digit = x % 10;
            if (result > max10 || (result == max10 && digit > lastDigitOfMaximum)
                    || result < min10 || (result == min10 && digit < lastDigitOfMinimum)) {
                return 0;
            }
            result = result * 10 + digit;
            x /= 10;
        }
        return result;
    }
}
