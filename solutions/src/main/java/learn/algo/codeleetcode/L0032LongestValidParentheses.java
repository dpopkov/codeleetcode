package learn.algo.codeleetcode;

import java.util.ArrayDeque;
import java.util.Deque;

/*
Given a string containing just the characters '(' and ')',
return the length of the longest valid (well-formed) parentheses substring.
 */
public class L0032LongestValidParentheses {

    // TC: O(n)
    // SC: O(n)
    public static int longestValidParentheses(String s) {
        int longest = 0;
        Deque<Integer> stack = new ArrayDeque<>();
        stack.push(-1); // Set initial boundary.
        final int len = s.length();
        for (int i = 0; i < len; i++) {
            char ch = s.charAt(i);
            if ('(' == ch) {
                stack.push(i);
            } else {    // There can be only ')'
                stack.pop(); // Remove index of opening parenthesis or current boundary.
                if (stack.isEmpty()) {
                    // There is no boundary.
                    // It means that this current character is not a part of a valid substring.
                    stack.push(i);
                } else {
                    // If the stack is not empty at this point it means that this
                    // is a part of a valid substring.
                    int current = i - stack.peek();
                    longest = Math.max(current, longest);
                }
            }
        }
        return longest;
    }
}
