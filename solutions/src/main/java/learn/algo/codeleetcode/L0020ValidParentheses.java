package learn.algo.codeleetcode;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Map;

/*
Given a string s containing just the characters '(', ')', '{', '}', '[' and ']',
determine if the input string is valid.

An input string is valid if:
    Open brackets must be closed by the same type of brackets.
    Open brackets must be closed in the correct order.
 */
public class L0020ValidParentheses {

    private static final Map<Character, Character> pairs = Map.of(
            ')', '(',
            '}', '{',
            ']', '['
    );

    // TC: O(n), SC: O(n)
    public boolean isValid(String s) {
        Deque<Character> stack = new ArrayDeque<>();
        for (int i = 0; i < s.length(); i++) {
            Character current = s.charAt(i);
            Character opening = pairs.get(current);
            if (opening == null) {  // current is Opening
                stack.push(current);
            } else {                // current is Closing
                if (stack.isEmpty()) {
                    return false;
                }
                if (opening.equals(stack.peek())) {
                    stack.pop();
                } else {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }
}
