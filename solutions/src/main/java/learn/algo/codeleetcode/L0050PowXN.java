package learn.algo.codeleetcode;

/*
 * Implement pow(x, n), which calculates x raised to the power n (i.e., x^n).
 */
public class L0050PowXN {

    /*
     *  Time complexity: O(log2(n))
     */
    public double myPow(double x, int n) {
        double r = 1.0;
        long p = n;
        if (n < 0) {
            p = -1 * p;
        }
        while (p > 0) {
            if (p % 2 == 0) {
                x = x * x;
                p = p / 2;
            } else {
                r = r * x;
                p = p - 1;
            }
        }
        if (n < 0) {
            r = 1.0 / r;
        }
        return r;
    }
}
