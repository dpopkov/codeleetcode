package learn.algo.codeleetcode;

/*
Given a string s, return the longest palindromic substring in s.
 */
public class L0005LongestPalindromicSubstring {

    // Solution uses "expand from center" approach
    // TC: O(n^2), SC: O(1)
    public String longestPalindrome(String s) {
        int start = 0;
        int end = 0;
        int centerLeft = 0;
        int centerRight = 0;
        while (centerRight < s.length()) {
            int left = centerLeft;
            int right = centerRight;
            while (left >= 0 && right < s.length() && s.charAt(left) == s.charAt(right)) {
                left--;
                right++;
            }
            left++;
            right--;
            if (end - start + 1 < right - left + 1) {
                start = left;
                end = right;
            }
            if (centerLeft == centerRight) {
                centerRight++;
            } else {
                centerLeft++;
            }
        }
        return s.substring(start, end + 1);
    }
}
