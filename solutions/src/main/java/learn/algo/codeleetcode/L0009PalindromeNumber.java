package learn.algo.codeleetcode;

/*
Given an integer x, return true if x is palindrome integer.
An integer is a palindrome when it reads the same backward as forward.
For example, 121 is a palindrome while 123 is not.
 */
public class L0009PalindromeNumber {

    public boolean isPalindrome(int x) {
        if (x == 0) {
            return true;
        }
        if (x < 0 || x % 10 == 0) {
            return false;
        }
        int rev = 0;
        while (rev < x) {
            int d = x % 10;
            rev = rev * 10 + d;
            x /= 10;
        }
        if (rev == x) {
            return true;
        }
        rev /= 10;
        return rev == x;
    }
}
