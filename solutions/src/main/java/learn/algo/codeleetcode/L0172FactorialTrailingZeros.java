package learn.algo.codeleetcode;

/*
Given an integer n, return the number of trailing zeroes in n!.
Note that n! = n * (n - 1) * (n - 2) * ... * 3 * 2 * 1.
 */
public class L0172FactorialTrailingZeros {

    public int trailingZeroes(int n) {
        /*
         * Formula of finding the number of trailing zeroes:
         *          n/5 + n/25 + n/125 + n/625 + ...
         * Time complexity: O(log5(n))
         * Space complexity: O(1)
         */
        int count = 0;
        for (int divisor = 5; divisor <= n; divisor *= 5) {
            count += n / divisor;
        }
        return count;
    }
}
