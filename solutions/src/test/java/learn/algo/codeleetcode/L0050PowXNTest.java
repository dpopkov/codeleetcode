package learn.algo.codeleetcode;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class L0050PowXNTest {

    private static Stream<Arguments> testData() {
        return Stream.of(
                Arguments.of(2.0, 3, 8.0),
                Arguments.of(2.0, 10, 1024.0),
                Arguments.of(2.1, 3, 9.261),
                Arguments.of(2, -2, 0.25),
                Arguments.of(-100.0, 2, 10000.0),
                Arguments.of(1.0, Integer.MAX_VALUE, 1.0),  // Test for Time Limit Exceeded
                Arguments.of(2.0, Integer.MIN_VALUE, 0.0)     // Test for Time Limit Exceeded
        );
    }

    @ParameterizedTest
    @MethodSource("testData")
    void myPow(double x, int n, double expected) {
        double actual = new L0050PowXN().myPow(x, n);
        assertEquals(expected, actual, 1e-14);
    }
}
