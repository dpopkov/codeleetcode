package learn.algo.codeleetcode;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class L0009PalindromeNumberTest {

    private static Stream<Arguments> testData() {
        return Stream.of(
                Arguments.of(0, true),
                Arguments.of(121, true),
                Arguments.of(1221, true),
                Arguments.of(1234321, true),
                Arguments.of(-121, false),
                Arguments.of(10, false),
                Arguments.of(1410110141, true)
        );
    }

    @ParameterizedTest
    @MethodSource("testData")
    void isPalindrome(int input, boolean expected) {
        boolean actual = new L0009PalindromeNumber().isPalindrome(input);
        assertEquals(actual, expected);
    }
}
