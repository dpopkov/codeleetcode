package learn.algo.codeleetcode;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class L0032LongestValidParenthesesTest {

    @ParameterizedTest
    @CsvSource({",0", "(),2", "()),2", "((),2", "()()),4", "(()(),4", "()((),2", "())((()),4", ")()()),4", "(()))())(,4"})
    void longestValidParentheses(String input, int expected) {
        input = input != null ? input : "";
        int actual = L0032LongestValidParentheses.longestValidParentheses(input);
        assertEquals(expected, actual);
    }
}
