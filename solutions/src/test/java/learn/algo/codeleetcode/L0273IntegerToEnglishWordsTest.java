package learn.algo.codeleetcode;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class L0273IntegerToEnglishWordsTest {

    @ParameterizedTest
    @CsvSource({
            "0,Zero",
            "1,One",
            "2,Two",
            "12,Twelve",
            "23,Twenty Three",
            "123,One Hundred Twenty Three",
            "929,Nine Hundred Twenty Nine",
            "1000,One Thousand",
            "1001,One Thousand One",
            "1038,One Thousand Thirty Eight",
            "1238,One Thousand Two Hundred Thirty Eight",
            "12345,Twelve Thousand Three Hundred Forty Five",
            "21238,Twenty One Thousand Two Hundred Thirty Eight",
            "99999,Ninety Nine Thousand Nine Hundred Ninety Nine",
            "999999,Nine Hundred Ninety Nine Thousand Nine Hundred Ninety Nine",
            "1000000,One Million",
            "1234567,One Million Two Hundred Thirty Four Thousand Five Hundred Sixty Seven",
            "999234567,Nine Hundred Ninety Nine Million Two Hundred Thirty Four Thousand Five Hundred Sixty Seven",
            "1000000000,One Billion",
            "1999234567,One Billion Nine Hundred Ninety Nine Million Two Hundred Thirty Four Thousand Five Hundred Sixty Seven",
            "2147483647,Two Billion One Hundred Forty Seven Million Four Hundred Eighty Three Thousand Six Hundred Forty Seven"
    })
    void numberToWords(int input, String expected) {
        String actual = L0273IntegerToEnglishWords.numberToWords(input);
        assertEquals(expected, actual);
    }
}
