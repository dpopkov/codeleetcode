package learn.algo.codeleetcode;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class L0005LongestPalindromicSubstringTest {

    private static Stream<Arguments> testData() {
        return Stream.of(
                Arguments.of("a", "a"),
                Arguments.of("ab", "a"),
                Arguments.of("aab", "aa"),
                Arguments.of("abb", "bb"),
                Arguments.of("baab", "baab"),
                Arguments.of("baaab", "baaab"),
                Arguments.of("baba", "bab"),
                Arguments.of("babad", "bab"),
                Arguments.of("babaabd", "baab"),
                Arguments.of("cbbd", "bb")
        );
    }

    @ParameterizedTest
    @MethodSource("testData")
    void longestPalindrome(String input, String expected) {
        String actual = new L0005LongestPalindromicSubstring().longestPalindrome(input);
        assertEquals(expected, actual);
    }
}
