package learn.algo.codeleetcode;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class L0172FactorialTrailingZerosTest {

    private static Stream<Arguments> testData() {
        return Stream.of(
                Arguments.of(3, 0),
                Arguments.of(5, 1),
                Arguments.of(6, 1),
                Arguments.of(10, 2),
                Arguments.of(0, 0),
                Arguments.of(30, 7)
        );
    }

    @ParameterizedTest
    @MethodSource("testData")
    void trailingZeros(int input, int expected) {
        int actual = new L0172FactorialTrailingZeros().trailingZeroes(input);
        assertEquals(expected, actual);
    }
}
