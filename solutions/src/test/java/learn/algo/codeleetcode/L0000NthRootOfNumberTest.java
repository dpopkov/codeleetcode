package learn.algo.codeleetcode;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class L0000NthRootOfNumberTest {

    private static Stream<Arguments> testData() {
        return Stream.of(
                Arguments.of(27.0, 3, 3.0),
                Arguments.of(7.0, 3, 1.913)
        );
    }

    @ParameterizedTest
    @MethodSource("testData")
    void nthRoot(double x, int n, double expected) {
        double actual = new L0000NthRootOfNumber().nthRoot(x, n);
        assertEquals(expected, actual, L0000NthRootOfNumber.ALLOWED_ERROR);
    }
}
