package learn.algo.codeleetcode;

import learn.algo.codeleetcode.utils.CollectionsUtils;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertTrue;

class L0049GroupAnagramsTest {

    private static Stream<Arguments> testData() {
        return Stream.of(
                Arguments.of(new String[] {"eat","tea","tan","ate","nat","bat"}, List.of(List.of("bat"), List.of("tan", "nat"), List.of("ate","eat","tea"))),
                Arguments.of(new String[] {""}, List.of(List.of(""))),
                Arguments.of(new String[] {"a"}, List.of(List.of("a")))
        );
    }

    @ParameterizedTest
    @MethodSource("testData")
    void groupAnagrams(String[] input, List<List<String>> expected) {
        List<List<String>> actual = new L0049GroupAnagrams().groupAnagrams(input);
        assertTrue(CollectionsUtils.deepEquals(expected, actual));
    }
}
