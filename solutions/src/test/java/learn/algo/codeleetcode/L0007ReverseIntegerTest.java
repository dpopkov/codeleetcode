package learn.algo.codeleetcode;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class L0007ReverseIntegerTest {

    private static Stream<Arguments> testData() {
        return Stream.of(
                Arguments.of(12, 21),
                Arguments.of(123, 321),
                Arguments.of(-123, -321),
                Arguments.of(120, 21),
                Arguments.of(Integer.MIN_VALUE, 0),
                Arguments.of(Integer.MAX_VALUE, 0)
        );
    }

    @ParameterizedTest
    @MethodSource("testData")
    void reverse(int input, int expected) {
        int actual = new L0007ReverseInteger().reverse(input);
        assertEquals(expected, actual);
    }
}
