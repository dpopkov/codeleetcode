package learn.algo.codeleetcode;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class L0020ValidParenthesesTest {

    private static Stream<Arguments> testData() {
        return Stream.of(
                Arguments.of("()", true),
                Arguments.of("(", false),
                Arguments.of(")", false),
                Arguments.of("{[]}", true),
                Arguments.of("()[]{}", true),
                Arguments.of("(]", false),
                Arguments.of("(])", false)
        );
    }

    @ParameterizedTest
    @MethodSource("testData")
    void valid(String input, boolean expected) {
        boolean actual = new L0020ValidParentheses().isValid(input);
        assertEquals(expected, actual);
    }
}
