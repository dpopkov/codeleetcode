package learn.algo.codeleetcode.utils;

import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class CollectionsUtilsTest {

    @Test
    void testDeepEquals() {
        Collection<Set<String>> c1 = List.of(Set.of("1", "2"), Set.of("3"));
        Collection<Set<String>> c2 = List.of(Set.of("3"), Set.of("1", "2"));
        boolean result = CollectionsUtils.deepEquals(c1, c2);
        assertTrue(result);
    }

    @Test
    void testDeepNotEquals() {
        Collection<Set<String>> c1 = List.of(Set.of("1", "2"), Set.of("3", "4"));
        Collection<Set<String>> c2 = List.of(Set.of("3"), Set.of("1", "2"));
        boolean result = CollectionsUtils.deepEquals(c1, c2);
        assertFalse(result);
    }
}
