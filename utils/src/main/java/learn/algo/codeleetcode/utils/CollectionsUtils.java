package learn.algo.codeleetcode.utils;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class CollectionsUtils {

    /** Checks whether the specified collections have the same content
     * at all levels and in any order. */
    public static <T> boolean deepEquals(Collection<T> a, Collection<T> b) {
        if (a.size() != b.size()) {
            return false;
        }
        Set<Object> aSet = convertToSet(a);
        Set<Object> bSet = convertToSet(b);
        return aSet.equals(bSet);
    }

    private static Set<Object> convertToSet(Collection<?> collection) {
        Set<Object> set = new HashSet<>();
        for (Object item : collection) {
            if (item instanceof Collection) {
                Set<Object> s = convertToSet((Collection<?>) item);
                set.add(s);
            } else {
                set.add(item);
            }
        }
        return set;
    }
}
